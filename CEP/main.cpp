#include <can.h>
#include <gpio.h>
#include <main.h>
#include <rtc.h>
#include <usart.h>

#include "main_application.h"

void log(const std::string &msg) {
  HAL_UART_Transmit(&huart1, reinterpret_cast<const uint8_t *>(msg.data()),
                    msg.size(), 1000);
}

int main() {
  HAL_Init();

  // Make sure that SYSCLK isn't set to PLL before configuring it.
  CLEAR_BIT(RCC->CFGR, RCC_CFGR_SW);

  //    __enable_irq();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize program */
  MainApplication app;
  app.Init();
  if (app.DoPost()) {
    app.Run();
  }

  Error_Handler();

  return 0;
}
