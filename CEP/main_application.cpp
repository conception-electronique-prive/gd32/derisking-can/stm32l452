/*
 * main_application.cpp
 *
 *  Created on: Oct 13, 2022
 *      Author: Utilisateur
 */

#include "main_application.h"

#include <can.h>
#include <gpio.h>
#include <rtc.h>
#include <usart.h>

#include <services/logger.hpp>

MainApplication::MainApplication() = default;

static CanModule *s_can = nullptr;

void MainApplication::Init() {
  InitializeHal();
  InitializeModule();
}

void MainApplication::Run() {
  static constexpr uint32_t period = 2000;
  static uint32_t deadline = HAL_GetTick();
  auto *logger = Logger::Get();
  logger->Log("Run\r\n");
  for (;;) {
    m_can->Run();
    m_uart->Run();
    auto now = HAL_GetTick();
    if (m_can->GetNumberOfAvailableFrames() != 0) {
      deadline = now + period;
      auto frame = m_can->ReceiveFrame();
      logger->Log("Received: [%d]", frame.frame.StdId);
      for (auto c : frame.data) {
        logger->Log(" %02x", c);
      }
      logger->Log("\r\n");
      if (frame.frame.StdId == 0 && frame.data.size() == 8 &&
          frame.data[0] == 1 && frame.data[1] == 2 && frame.data[2] == 3 &&
          frame.data[3] == 4 && frame.data[4] == 5 && frame.data[5] == 6 &&
          frame.data[6] == 7 && frame.data[7] == 8) {
        logger->Log("Repeat\r\n");
        m_can->TransmitFrame(frame.frame.StdId, frame.data.data(),
                             frame.data.size());
      }
    }
    if (now > deadline) {
      deadline = now + period;
      logger->Log("Starting %d\r\n", now);
      m_can->TransmitFrame(0, {1, 2, 3, 4, 5, 6, 7, 8});
    }
  }
}

void MainApplication::InitializeHal() {
  MX_GPIO_Init();
  MX_RTC_Init();
  MX_CAN1_Init();
  MX_USART1_UART_Init();
}

void MainApplication::InitializeModule() {
  new RtcModule(&hrtc, "RTC");
  m_uart = new UartModuleIt(&huart1, "UART");
  new Logger(m_uart);
  Logger::Get()->Log("\033c");
  Logger::Get()->Log("L452\r\n");

  m_can = new CanModule(&hcan1, "CAN");
  s_can = m_can;

  CEP_CAN::FilterConfiguration filter;
  m_can->ConfigureFilter(filter);

  for (auto c :
       {CEP_CAN::Irq::TxMailboxEmpty, CEP_CAN::Irq::Fifo0MessagePending,
        CEP_CAN::Irq::Fifo0Full, CEP_CAN::Irq::Fifo0Overrun,
        CEP_CAN::Irq::Fifo1MessagePending, CEP_CAN::Irq::Fifo1Full,
        CEP_CAN::Irq::Fifo1Overrun, CEP_CAN::Irq::Wakeup,
        CEP_CAN::Irq::SleepAck, CEP_CAN::Irq::ErrorWarning,
        CEP_CAN::Irq::ErrorPassive, CEP_CAN::Irq::BusOffError,
        CEP_CAN::Irq::LastErrorCode, CEP_CAN::Irq::ErrorStatus}) {
    m_can->EnableInterrupt(c);
  }
}

extern "C" {
void CanCallback() { s_can->HandleIrq(); }
}
