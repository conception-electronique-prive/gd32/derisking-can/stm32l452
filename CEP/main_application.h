/*
 * main_application.h
 *
 *  Created on: Oct 13, 2022
 *      Author: Utilisateur
 */

#ifndef MAIN_APPLICATION_H_
#define MAIN_APPLICATION_H_

#include <drivers/CanModule.hpp>
#include <drivers/uart/it.hpp>
#include <processes/application.hpp>

class MainApplication : public cep::Application {
public:
  MainApplication();

  void Init() final;
  bool DoPost() final { return true; }
  void Run();

private:
  void InitializeHal();
  void InitializeModule();

private:
  CanModule *m_can;
  UartModuleIt *m_uart;
};

#endif /* MAIN_APPLICATION_H_ */
